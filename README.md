# SlrrDurr

The Slack sibling of the Durr family.

His laziness is magnificent, so he only moves to do a single thing: manage slack admins - yet does it with pride.

## Usage

###  Arguments

- **-config** the configuration file to use, by default SlrrDurr will load *slrrdurr.yml* in the current working directory.
- **-dryrun** don't actually change anything, only evaluates which changes should happen.
- **-version** prints the version and exits without error.

### Required Environment Variables

- **SLACK_TOKEN** the token to use when contacting the Slack API.

### API Token scope

You will need a [user type](https://api.slack.com/docs/token-types) slack token. This user needs to belong to the [admin scope](https://api.slack.com/scopes).

## Configuration

Configuration is managed through a `yaml` file. This file declares the admin list for SlrrDurr to _meanly_ collapse reality into.

### Concepts

SlrrDurr understands 1 basic element that it uses to add slack users to the admin scope, and caringly kick errant users out.

### Full Sample

```yaml
---
admins:
 - yak_shaver
 - wood_chopper
 - lorry_driver
 - sunglasses_jeff
```
