package main

import (
	"flag"
	"fmt"
	"os"
	"sort"
	"strings"

	"github.com/nlopes/slack"
	"github.com/sirupsen/logrus"
	"gitlab.com/yakshaving.art/slrrdurr/version"
)

func main() {
	logrus.SetFormatter(&logrus.TextFormatter{
		DisableTimestamp: true,
	})

	args := Args{
		SlackToken: os.Getenv("SLACK_TOKEN"),
	}

	flag.BoolVar(&args.Debug, "debug", false, "enable debug mode")
	flag.BoolVar(&args.Version, "version", false, "show version and quit")

	flag.Parse()

	if args.Version {
		fmt.Println(version.GetVersion())
		os.Exit(0)
	}

	if args.Debug {
		logrus.SetLevel(logrus.DebugLevel)
	}

	args.Must()

	client := slack.New(args.SlackToken)
	users, err := client.GetUsers()
	if err != nil {
		logrus.Fatalf("failed to fetch users: %s", err)
	}
	sort.Slice(users, func(i, j int) bool {
		return users[i].Name < users[j].Name
	})

	fmt.Println("Users:")
	for _, u := range users {
		fmt.Println(" ", userWithDetails(u))
	}

}

func userWithDetails(u slack.User) string {
	s := make([]string, 0)
	if u.IsPrimaryOwner {
		s = append(s, "primary owner")
	} else if u.IsOwner {
		s = append(s, "owner")
	}

	if u.IsAdmin {
		s = append(s, "admin")
	}

	if u.IsAppUser {
		s = append(s, "app")
	}

	if u.IsBot {
		s = append(s, "bot")
	}

	if u.IsStranger {
		s = append(s, "stranger")
	}
	if u.IsRestricted {
		s = append(s, "restricted")
	}
	if u.IsUltraRestricted {
		s = append(s, "ultra restricted")
	}

	username := u.Name
	if u.RealName != "" {
		username = fmt.Sprintf("%s (%s)", username, u.RealName)
	}

	if len(s) == 0 {
		return username
	}

	return fmt.Sprintf("%s [%s]", username, strings.Join(s, ", "))
}

type Args struct {
	SlackToken string

	Debug   bool
	Version bool
}

func (a Args) Must() {
	if a.SlackToken == "" {
		logrus.Fatalf("SLACK_TOKEN is mandatory")
	}
}
